/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package clientIO;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.Properties;

/**
 * @author: 陈磊 <br/>
 * Date: 12-12-21<br/>
 * Time: 下午3:56<br/>
 * connectMethod:13638363871@163.com<br/>
 */
public class IOtest implements Runnable { //创建一个连接
    private static final Logger LOG = LoggerFactory.getLogger(IOtest.class);
    private Socket socket;
    private int i = 0;

    public IOtest(Properties properties, Socket socket) {
        try {
            this.socket = socket;
            this.socket.connect(new InetSocketAddress(properties.getProperty("host"), Integer.valueOf(properties.getProperty("port"))));
            System.out.println("创建连接:" + this.socket);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * send message
     */
    public void send() throws InterruptedException {
        long t1 = 0;
        try {
            //数据包协议 包长+包体（消息内容）
            Thread.sleep(100);
            OutputStream out = this.socket.getOutputStream();
            DataOutputStream data = new DataOutputStream(out);
//            //11
//            String passport = "a";
//            String password = "123" + "game";
//            data.writeShort(passport.getBytes().length + MD5(password).getBytes().length + 4);//包长
//            data.writeByte(1);//消息类型
//            data.writeByte(1);
//            data.writeByte(passport.getBytes().length);
//            data.write(passport.getBytes());
//            data.writeByte(MD5(password).getBytes().length);
//            data.write(MD5(password).getBytes());//消息内容
//            data.flush();

//
//            System.out.println("MD5:"+MD5("123"));



//            31

//            PrivateSenceChat request=new PrivateSenceChat();
//            request.setRoomtype((byte) 0);
//            String message= JSON.toJSONString(request);
//            request=null;
//
//            data.writeShort(message.getBytes().length+1);
//            data.writeByte(-1);//消息类型
//            data.write(message.getBytes());
//            data.flush();


//            //  -1
//            data.writeShort(13);
//            data.writeByte(-1);
//            data.writeInt(1);
//            data.writeInt(896);
//            data.writeInt(896);
//            data.flush();

//            //  -4
//            data.writeShort(9);
//            data.writeByte(-4);
//            data.writeInt(896);
//            data.writeInt(896);
//            data.flush();

//            //  -5
//            data.writeShort(2);
//            data.writeByte(-1);
//            data.writeByte(0);
//            data.flush();
//            //  -6
//            data.writeShort(9);
//            data.writeByte(-6);
//            data.writeInt(896);
//            data.writeInt(896);
//            data.flush();

            //  -7
//            data.writeShort(5);
//            data.writeByte(-7);
//            data.writeInt(1);
//            data.flush();

//            System.out.println(MD5(password));


//            SysRequest request = new SysRequest();
//            request.setFrequency(1);
//            String str = JSON.toJSONString(request);
////
//            //-60测试 json数据
//            data.writeShort(str.getBytes().length + 1);
//            data.writeByte(-62);
//            data.write(str.getBytes());
//            data.flush();
////
//            t1 = System.currentTimeMillis();
     //       System.out.println("---------------");
//            DataInputStream dataInputStream = new DataInputStream(this.socket.getInputStream());
////
//            int len=dataInputStream.readShort();
//            byte[]repack=new byte[len];
//            dataInputStream.read(repack,0,len);
//                LOG.info("TIME(纳秒):" + ((System.currentTimeMillis()) - t1) + "/ms");
//                LOG.info("packlength:"+len);
//                LOG.info("客户端收到消息类型:"+repack[0]);
//                LOG.info("客户端收到消息类型:"+repack[1]);
//                LOG.info("客户端收到消息类型:"+repack[2]);
//                LOG.info("客户端收到消息:"+repack[2]+":"+ Tools.ByteToInt(3, 6, repack));


        } catch (IOException e) {
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                send();
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }



    /**
     * MD5 加密方法 返回32位String
     *
     * @param text 需要加密的数据
     */
    private String MD5(String text) {
        StringBuffer md5 = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());
            byte[] digest = md.digest();
            for (int i = 0; i < digest.length; i++) {
                md5.append(Character.forDigit((digest[i] & 0xF0) >> 0x4, 0x10));
                md5.append(Character.forDigit((digest[i] & 0xF), 0x10));
            }
        } catch (Exception e) {
        }
        return md5.toString();
    }
}
