/*
 * Copyright (c) 2015.
 * 游戏服务器核心代码编写人石头哥哥拥有使用权
 * 最终使用解释权归创心科技所有
 * 联系方式：E-mail:13638363871@163.com ;
 * 个人博客主页：http://my.oschina.net/chenleijava
 * powered by 石头哥哥
 */

package com.google.flatbuffers;

// Class that holds shared constants.

public class Constants {
    // Java doesn't seem to have these.
    static final int SIZEOF_SHORT = 2;
    static final int SIZEOF_INT = 4;
    static final int FILE_IDENTIFIER_LENGTH = 4;
}

