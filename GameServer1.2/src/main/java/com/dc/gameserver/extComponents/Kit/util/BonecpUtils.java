package com.dc.gameserver.extComponents.Kit.util;//package com.dc.gameserver.extComponents.utilsKit.util;
//
//import com.jolbox.bonecp.BoneCP;
//import com.jolbox.bonecp.BoneCPConfig;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
///**
// * Created with IntelliJ IDEA.
// * User: CHENLEI
// * Date: 12-11-13
// * Time: 下午9:38
// * To change this template use File | Settings | File Templates.
// * bonecp
// */
//public class BonecpUtils {
//    private static final  String url="jdbc:mysql://127.0.0.1:3306/game1.2?useUnicode=true&amp;characterEncoding=UTF-8";
//    private static final  String username="root";
//    private static final  String password="123456";
//    private static final BonecpUtils bonecpUtils=new BonecpUtils();
//    private static BoneCP connectionPool = null;
//
//    private BonecpUtils(){
//    }
//
//    /**
//     * 获取引用
//     * @return
//     */
//    public static BonecpUtils getBonecp(){
//        return    bonecpUtils;
//    }
//    /**
//     * 初始化boncp
//     */
//       public void IntiBoncp(){
//           try {
//
//               Class.forName("com.mysql.jdbc.Driver");
//               BoneCPConfig config = new BoneCPConfig();
//               config.setJdbcUrl(url); // jdbc url specific to your database, eg jdbc:mysql://127.0.0.1/yourdb
//               config.setUsername(username);
//               config.setPassword(password);
//               config.setAcquireIncrement(5);
//               config.setMinConnectionsPerPartition(5);
//               config.setMaxConnectionsPerPartition(15);
//               config.setPartitionCount(3);
//               connectionPool = new BoneCP(config); // setup the connection pool
//           } catch (ClassNotFoundException e1) {
//               e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//           } catch (SQLException e) {
//               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//           }
//       }
//
//    /**
//     * 获取连接
//     * @return
//     * @throws java.sql.SQLException
//     */
//      public Connection getConnetion() throws SQLException {
//         if(connectionPool!=null)return connectionPool.getConnection();
//          return null;
//      }
//
//    /**
//     * 回收连接
//     * @param connection
//     * @throws java.sql.SQLException
//     */
//    public void closeConnection(Connection connection) throws SQLException {
//         connection.close();
//    }
//}
