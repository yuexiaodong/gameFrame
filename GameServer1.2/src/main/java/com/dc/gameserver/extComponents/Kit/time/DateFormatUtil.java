/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.time;

import org.apache.commons.lang.time.DateFormatUtils;

import java.sql.Timestamp;

/**
 * @author :陈磊 <br/>
 *         Project:CreazyGameServer1.2
 *         Date: 13-3-18
 *         Time: 下午9:52
 *         connectMethod:13638363871@163.com<br/>
 */
public class DateFormatUtil {


    /**
     * formate time yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getDateFormatA(){
        long nowTime = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(nowTime);
        return DateFormatUtils.format(timestamp, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * formate time HH:mm:ss
     * @return
     */
    public static String getDateFormatB(){
        long nowTime = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(nowTime);
        return DateFormatUtils.format(timestamp, "HH:mm:ss");
    }

}
