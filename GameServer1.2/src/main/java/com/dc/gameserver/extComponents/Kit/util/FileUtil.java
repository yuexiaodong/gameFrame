/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.util;

import java.io.*;

/**
 * @author : 陈磊 <br/>
 *         Date: 13-3-6<br/>
 *         Time: 上午1:16<br/>
 *         connectMethod:13638363871@163.com<br/>
 */
public class FileUtil {
    /**
     * copy file
     * @param sourceFileUrl
     * @throws java.io.IOException
     */
    private  void copyFile(String sourceFileUrl) throws IOException {
        try {
            String javaHome = System.getProperty("java.home");
            int bantered;
            File oldfield = new File(sourceFileUrl);
            String jrePath = javaHome+"/bin/"+oldfield.getName();
            if (oldfield.exists()) {
                InputStream inStream1 = new FileInputStream(sourceFileUrl);
                FileOutputStream fs1 = new FileOutputStream(jrePath);
                byte[] buffer = new byte[1024];
                while ( (bantered = inStream1.read(buffer)) != -1) {
                    fs1.write(buffer, 0, bantered);
                }

                inStream1.close();
                fs1.close();


            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
