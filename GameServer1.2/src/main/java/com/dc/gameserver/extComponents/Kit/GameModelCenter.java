package com.dc.gameserver.extComponents.Kit;//package com.dc.gameserver.extComponents.utilsKit;
//
//import Config;
//import com.dc.gameserver.serverCore.model.Gamemodel.Model;
//import javolution.util.FastMap;
//import jxl.Sheet;
//import jxl.Workbook;
//import jxl.read.biff.BiffException;
//import org.apache.log4j.xml.DOMConfigurator;
//import org.dom4j.Attribute;
//import org.dom4j.Document;
//import org.dom4j.Element;
//import org.dom4j.io.SAXReader;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.lang.reflect.Field;
//import java.lang.reflect.Method;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.Map;
//
///**
// * @author : 石头哥哥
// *         Project : LandlordsServer1.0
// *         Date: 13-7-5
// *         Time: 下午4:48
// *         Connect: 13638363871@163.com
// *         packageName: Server.ServerCore.Model.model
// *         缓存游戏配置信息
// */
//@SuppressWarnings("unchecked")
//@Deprecated
//public class GameModelCenter {
//    private static final Logger LOG = LoggerFactory.getLogger(GameModelCenter.class);
//    public final static SimpleDateFormat FORMAT;
//
//    static {
//        FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    }
//
//
//    ////////////////////任务///////////////////////////////////////////////////////////////////////////////////////////////////  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    /**任务存储容器***/
////    public static Map<Integer, TaskModel>taskModelFastMap = new FastMap<Integer,TaskModel>().shared();
////    public static Map<Integer, TaskModelDec>taskModelDecFastMap = new FastMap<Integer,TaskModelDec>().shared();
////    public static Map<Integer, DayTaskModel>daytaskModelFastMap = new FastMap<Integer,DayTaskModel>().shared();
//
//
//
//
//
//    /**
//     * 加载model配置文件
//     */
//    public void loads(){
//        /***初始化房间model*/
//
//    }
//
//
//
//    /**
//     * 初始化任务 系统（成就列表）
//     *  初始化日常任务
//     * @return
//     */
//    public boolean initTaskAndDayTask() {
//        boolean status = true;
//        try {
//            Workbook workbook = Workbook.getWorkbook(new FileInputStream( Config.DEFAULT_VALUE.FILE_PATH.TASK_CONFIG));
//
////            //成就任务model
////            Sheet sheet = workbook.getSheet(0);//xsl信息位置
////            int rows = sheet.getRows();       //总行数
////            int currentRow = 1;             //当前数据行    注意是从0开始的
////            TaskModel taskmodel;
////            for (int row=currentRow;row!=rows;++row){ //行
////                taskmodel=new TaskModel();
////                taskmodel.setId(Integer.parseInt(sheet.getCell(0, row).getContents().trim()));
////                taskmodel.setTaskName(sheet.getCell(1, row).getContents().trim());
////                taskmodel.setTasktype(Integer.parseInt(sheet.getCell(2, row).getContents().trim()));
////                taskmodel.setComplateNum("0/"+Integer.parseInt(sheet.getCell(3, row).getContents().trim()));
////                taskmodel.setDes(sheet.getCell(4, row).getContents().trim());
////                taskmodel.setGold(Integer.parseInt(sheet.getCell(5, row).getContents().trim()));
////                taskmodel.setTimelimit(Byte.parseByte(sheet.getCell(6, row).getContents().trim()));
////                taskModelFastMap.put(taskmodel.getId(),taskmodel);
////            }
////
////            //成就任务分类说明model
////            sheet=workbook.getSheet(1);
////            rows=sheet.getRows();
////            currentRow=1;
////            TaskModelDec taskModelDec;
////            for (int row=currentRow;row!=rows;++row){
////                taskModelDec=new TaskModelDec();
////                taskModelDec.setId(Integer.parseInt(sheet.getCell(0, row).getContents().trim()));
////                taskModelDec.setTasktype(sheet.getCell(1, row).getContents().trim());
////                taskModelDec.setDes(sheet.getCell(2, row).getContents().trim());
////                taskModelDecFastMap.put(taskModelDec.getId(),taskModelDec);
////            }
////
////            //日常任务model
////            sheet=workbook.getSheet(2);
////            rows=sheet.getRows();
////            currentRow=1;
////            DayTaskModel dayTaskModel;
////            for (int row=currentRow;row!=rows;++row){
////                dayTaskModel=new DayTaskModel();
////                dayTaskModel.setId(Integer.parseInt(sheet.getCell(0, row).getContents().trim()));
////                dayTaskModel.setTasktype(Integer.parseInt(sheet.getCell(2, row).getContents().trim()));
////                dayTaskModel.setComplateNum("0/"+Integer.parseInt(sheet.getCell(3, row).getContents().trim()));
////                dayTaskModel.setDes(sheet.getCell(4, row).getContents().trim());
////                dayTaskModel.setGold(Integer.parseInt(sheet.getCell(5, row).getContents().trim()));
////                dayTaskModel.setCd((byte)24);
////                daytaskModelFastMap.put(dayTaskModel.getId(),dayTaskModel);
////            }
//
//            workbook.close();
//        } catch (IOException e) {
//            LOG.error("初始化任务（成就列表）失败"+e);
//        } catch (BiffException e) {
//            LOG.error("初始化任务（成就列表）失败"+e);
//        }
//
//        return status;
//    }
//
//
//    /**
//     * 初始化   w物品
//     * @return
//     */
//    public boolean  initGoods(){
//        boolean status=false;
//        try {
//            Workbook workbook = Workbook.getWorkbook(new FileInputStream(Config.DEFAULT_VALUE.FILE_PATH.GOODS_CONFIG));
//
//            //道具
//            Sheet sheet = workbook.getSheet(0);//xsl信息位置
//            int rows = sheet.getRows();       //总行数
//            int currentRow = 1;             //当前数据行    注意是从0开始的
////            PropsModel                      propsModel;
////            for (int row=currentRow;row!=rows;++row){
////                propsModel=new PropsModel();
////                propsModel.setId(Integer.parseInt(sheet.getCell(0, row).getContents().trim()));
////                propsModel.setName(sheet.getCell(1, row).getContents().trim());
////                propsModel.setBuyMethod(Byte.parseByte(sheet.getCell(2, row).getContents().trim()));
////                propsModel.setPrice(Integer.parseInt(sheet.getCell(3, row).getContents().trim()));
////                propsModel.setExpired(Integer.parseInt(sheet.getCell(4, row).getContents().trim()));
////                propsModel.setType(Byte.parseByte(sheet.getCell(5, row).getContents().trim()));
////                 propsModelFastMap.put(propsModel.getId(),propsModel);
////            }
////
////            //金币
////             sheet = workbook.getSheet(1);//xsl信息位置
////             rows = sheet.getRows();       //总行数
////             currentRow = 1;             //当前数据行    注意是从0开始的
////            GoldModel   goldModel;
////            for (int row=currentRow;row!=rows;++row){
////                goldModel=new GoldModel();
////                goldModel.setId(Integer.parseInt(sheet.getCell(0, row).getContents().trim()));
////                goldModel.setName(sheet.getCell(1, row).getContents().trim());
////                goldModel.setPrice(Integer.parseInt(sheet.getCell(2, row).getContents().trim()));
////                goldModel.setGolds(Integer.parseInt(sheet.getCell(3, row).getContents().trim()));
////                 goldModelFastMap.put(goldModel.getId(),goldModel);
////            }
////
////            //   vip
////            sheet = workbook.getSheet(2);//xsl信息位置
////            rows = sheet.getRows();       //总行数
////            currentRow = 1;             //当前数据行    注意是从0开始的
////            VipModel vipModel;
////            for (int row=currentRow;row!=rows;++row){
////                vipModel=new VipModel();
////                vipModel.setId(Integer.parseInt(sheet.getCell(0, row).getContents().trim()));
////                vipModel.setName(sheet.getCell(1, row).getContents().trim());
////                vipModel.setPrice(Integer.parseInt(sheet.getCell(2, row).getContents().trim()));
////                vipModel.setExpired(Integer.parseInt(sheet.getCell(3, row).getContents().trim()));
////                vipModelFastMap.put(vipModel.getId(),vipModel);
////            }
//            workbook.close();
//        } catch (IOException e) {
//            LOG.error("初始化（物品列表）失败" + e);
//        } catch (BiffException e) {
//            LOG.error("初始化（物品列表）失败" + e);
//        }
//        return  status;
//    }
//
//
//
//    /**
//     枚举系统基础配置信息
//     <Props>
//     <unit>
//     <id>1</id>
//     <name> 亚特兰蒂斯</name>
//     </unit>
//     </Props>
//     Scenes：modelType
//     base：game of  node
//     */
//    private enum readXML{
//        ;
////        propsModel("Props", PropsModel.class,propsModelFastMap),
////        goldsModel("Golds",GoldModel.class,goldModelFastMap),
////        vipsModel("VIPs",VipModel.class,vipModelFastMap),
//
//        private final String id;
//        private final Class<?> cls;
//        private  Map map;
//
//        private readXML(final String id,final Class<?> cls,Map map){
//            this.id = id;
//            this.cls= cls;
//            this.map = map;
//        }
//        public String getId() {
//            return id;
//        }
//        public Class<?> getCls() {
//            return cls;
//        }
//        public Map getMap() {
//            return map;
//        }
//    }
//
//    /**
//         <Props>
//         <unit>
//         <id>1</id>
//         <name> 亚特兰蒂斯</name>
//         </unit>
//         </Props>
//     * 解析xml
//     * @param sourcesXML
//     * @param unit
//     */
//    private void parseGoodsXML(String sourcesXML, String unit) {
//        Document doc = null;
//        try {
//            InputStream inputStream = new FileInputStream(sourcesXML);
//            SAXReader saxReader = new SAXReader();
//            doc = saxReader.read(inputStream);
//        } catch (Exception e) {
//            LOG.error("DocumentException", e);
//        }
//        if (doc != null) {
//            Element root = doc.getRootElement();
//            for(readXML xmlRoot: readXML.values()){
//                String flag=xmlRoot.getId();      // Props
//                Element element = root.element(flag);        //获取   Props下子节点
//                Map<String,String> map = new FastMap<String, String>();
//                if (element != null) {
//                    //迭代子节点 一个unit就是一个model实体对应
//                    for(Iterator vos = element.elementIterator(unit); vos.hasNext(); ) {
//                        Element vo = (Element) vos.next();
//                        //迭代unit下的子节点--->节点名称+value
//                        for(Iterator values = vo.elementIterator(); values.hasNext(); ) {
//                            Element value = (Element) values.next();
//                            map.put(value.getName(), (String)value.getData());
//                        }
//                        Object obj =run2Object(map, xmlRoot.getCls());
//                        Model model = (Model)obj;
////                        xmlRoot.getMap().put(model.getId(), model);
//                    }
//
//                }
//            }
//        }
//    }
//
//
//    /**
//     *
//     * @param sourcesXML
//     */
//    public static void IntiXmlConfig(String sourcesXML){
//        Document doc = null;
//        try {
//            InputStream inputStream = new FileInputStream(sourcesXML);
//            SAXReader saxReader = new SAXReader();
//            doc = saxReader.read(inputStream);
//        } catch (Exception e) {
//            LOG.error("DocumentException", e);
//        }
//        if (doc!=null){
//            //获取根节点
//            Element root = doc.getRootElement();
//             //迭代model节点
//             for(Iterator iterator=root.elementIterator();iterator.hasNext();){
//                 Element element= (Element) iterator.next();
//                 //迭代model属性
//                 for (Iterator vos = element.attributeIterator(); vos.hasNext();){
//                     Attribute attribute= (Attribute) vos.next();
//                     String name=attribute.getName();
//                     String value= attribute.getValue() ;
//                     System.out.println(name+":"+value);
//                 }
//             }
//        }
//    }
//
//    /**
//     * 通过反射，注入model属性
//     * @param map
//     * @param classType
//     * @return  Object
//     */
//    private Object run2Object(Map<String, String> map,Class classType){
//        Object objectCopy = null;
//        try {
//            objectCopy = classType.newInstance();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        // 获得对象的所有属性
//        Field[] fields = classType.getDeclaredFields();
//
//        for (Field field : fields) {
//            // 获取数组中对应的属性
//            String fieldName = field.getName();
//            String stringLetter = fieldName.substring(0, 1).toUpperCase();
//
//            // 获得相应属性的setXXX方法名称
//            String setName = "set" + stringLetter + fieldName.substring(1);
//            // 获取相应的setXXX方法
//            Method setMethod = null;
//            try {
//                setMethod = classType.getMethod(setName, new Class[] {field
//                        .getType()});     //根据方法名获取set方法
//            } catch (Exception e) {
//                continue;
//            }
//
//            // 将获取的数据注入到model中
//            try {
//                Class classes[]  = setMethod.getParameterTypes();
//                Object value = null;
//                String fieldValue = map.get(fieldName);    //对应的值
//                if(fieldValue == null || "".equals(fieldValue)){
//                     return null;
//                }else if(classes[0].equals(Date.class)){
//                    value = FORMAT.parse(fieldValue);
//                }else if(classes[0].equals(Integer.class)){
//                    value = Integer.valueOf(fieldValue);
//                }else if(classes[0].equals(Long.class)){
//                    value = Long.valueOf(fieldValue);
//                }else if(classes[0].equals(Float.class)){
//                    value = Float.valueOf(fieldValue);
//                }else if(classes[0].equals(Double.class)){
//                    value = Double.valueOf(fieldValue);
//                }else{
//                    value = fieldValue;
//                }
//                setMethod.invoke(objectCopy, value);
//            } catch (Exception ignored) {
//            }
//        }
//        return objectCopy;
//    }
//
//
//    public static void main(String[]args){
//        DOMConfigurator.configure(Config.DEFAULT_VALUE.FILE_PATH.LOG4J);
////        GameModelCenter gameDataCenter=new GameModelCenter();
////        gameDataCenter.loads();
//
//        IntiXmlConfig ("res/goods.xml");
//    }
//
//
//}
