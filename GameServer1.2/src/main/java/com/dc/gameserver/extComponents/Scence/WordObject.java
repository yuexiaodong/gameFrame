/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Scence;


import io.netty.buffer.ByteBuf;

/**
 * Created with IntelliJ IDEA.
 * User: CHENLEI
 * Date: 12-11-21
 * Time: 下午4:02
 * To change this template use File | Settings | File Templates.
 * game all word supper abstract class代码复用
 *  对象超类abstract
 */
public abstract  class WordObject {

    private int  ID;//对象ID
    private int mapId = 0; // 所在地图编号
    private int unitId = 0;// 当前的单元格ID

    private int typeId = 0;  //对象类型
    private int x = 0; // x坐标
    private int y = 0; // y坐标
    private int z = 0; // z坐标


    private short FaceTo = 0; // 面向

    private String Name = "";// 对象名字

    /**
     * 对象进入场景
     */
    public void intoSence() {
        int unitID=this.getMapArea().getMapUnitID(x,y);
        this.getMapArea().setMapStatus(MapStatus.INTO_MAP);
        this.getMapArea().checkLocation(this,unitID);
        this.setUnitId(unitID); //对象所在单元格编号
    }


    /**
     * 对象的位置坐标矫正
     * @param x
     * @param y
     */
    public void setLocation(int x, int y) {
        InitLocation(x,y);
        int nowAreaId = getMapArea().getMapUnitID(this.getX(), this.getY());//当前的区域
        getMapArea().setMapStatus(MapStatus.REDRESS_MAP);
        this.getMapArea().checkLocation(this, nowAreaId); //位置的变换
        this.setUnitId(nowAreaId);
    }


    /**
     * 对象离开场景
     */
    public void disappear() {
        this.getMapArea().setMapStatus(MapStatus.LEAVE_MAP);
        this.getMapArea().checkLocation(this,this.getUnitId()); //位置的变换
    }


    /**
     * 提交数据信息,用于广播(不向发送者广播)
     *
     * @param xobj 活动对象
     * @param buffer
     */
    public void submitSynMsg(WordObject xobj, ByteBuf buffer) {
//        MapManager.MapAres[this.mapId].getMapUnits()[this.unitId].submitSynMsg(
//                xobj, buffer);
    }

    /**
     * 获取地图对象
     * where's MapArea    ?
     * @return  MapArea
     */
    private MapArea getMapArea() {
        return MapManager.MapAres[this.getMapId()];
    }



    /**
     * 初始对象的位置坐标
     *
     * @param x
     * @param y
     */
    public void InitLocation(int x, int y) {
        this.setX(x);
        this.setY(y);
    }
    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
    public short getFaceTo() {
        return FaceTo;
    }

    public void setFaceTo(short faceTo) {
        FaceTo = faceTo;
    }

}
